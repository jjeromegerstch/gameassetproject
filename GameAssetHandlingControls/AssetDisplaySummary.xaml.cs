﻿using System.Windows.Controls;
using System.ComponentModel;
using System;
using System.IO;
using System.Windows.Media.Imaging;

namespace GameAssetHandlingControls
{
    /// <summary>
    /// Logique d'interaction pour AssetDisplaySummary.xaml
    /// </summary>
    public partial class AssetDisplaySummary : UserControl,INotifyPropertyChanged
    {
        private const string ThumbnailNotFoundPath = @"pack://application:,,,/GameAssetHandlingControls;component/Ressources/None_Thumbnail.png";

        private string _AssetDisplayName = "Name of the asset";

        public string AssetDisplayName
        {
            get { return _AssetDisplayName; }
            set {
                _AssetDisplayName = value;
                OnPropertyChanged(nameof(AssetDisplayName));
            }
        }

        public string ThumbnailPath
        {
            get { return _ThumbnailPath; }
            set
            {
                if (IsValidImage(value))
                    _ThumbnailPath = value;
                else
                    _ThumbnailPath = ThumbnailNotFoundPath;
                OnPropertyChanged(nameof(ThumbnailPath));
            }
        }

        bool IsValidImage(string filename)
        {
            if (File.Exists(filename))
            {
                try
                {
                    BitmapImage newImage = new BitmapImage(new Uri(filename));
                }
                catch (Exception)
                {
                    // System.NotSupportedException:
                    // No imaging component suitable to complete this operation was found.
                    return false;
                }
                return true;
            }
            return false;
        }

        private string _ThumbnailPath = ThumbnailNotFoundPath;

        private string _Version = "Version";

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public string Version
        {
            get { return _Version; }
            set {
                _Version = value;
                OnPropertyChanged(nameof(Version));
            }
        }
               
        public AssetDisplaySummary()
        {
            InitializeComponent();
            this.DataContext = this;
        }
    }
}
