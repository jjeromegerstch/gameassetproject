﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Game_Asset_Handler
{
	/// <summary>
	/// Logique d'interaction pour MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
        int currentImage = 1;

		public MainWindow()
		{
			InitializeComponent();
            //first.ThumbnailPath = @"/Ressources/None_Thumbnail.png";
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < 10000000000; i++)
            {
                currentImage++;

                if (currentImage > 4)
                    currentImage = 1;

                first.ThumbnailPath = @"S:\Institut_Artline\Projet Fin Annee\Icones_fantomes\" + currentImage + ".png";

                first.Version = "1." + currentImage;
                first.AssetDisplayName = "" + currentImage;
            }
           
        }
    }
}
